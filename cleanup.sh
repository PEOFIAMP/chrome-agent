#!/bin/bash
#
#   Copyright 2013 The University of Tokyo
#   
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#   
#   http://www.apache.org/licenses/LICENSE-2.0
#   
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

# Incomplete (if idp_name contains a bad string, this will behave badly)
IDP=`cat .idp_host_name`
if [ -n "$IDP" ]; then
  sed -i -e "s/$IDP/{{{idp_host_name}}}/" js/main.js
  sed -i -e "s/$IDP/{{{idp_host_name}}}/" manifest.json
fi

rm -f js/ext/inflate.js
rm -f js/ext/deflate.js
rm -f js/ext/yahoo-2.9.0-min.js
rm -rf js/ext/crypto-js/
rm -f CryptoJS.3.1.2.zip
find . -name "*~" | xargs rm -f
rm -f .idp_name

