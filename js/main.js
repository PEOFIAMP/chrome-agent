/*
 * Copyright 2013 The University of Tokyo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * A main javascript file for Agent protocol.
 * This inserts additional SAML information into AuthnRequest.
 *
 * Agent will override DH and Cascade algorithm when given AuthnRequest
 * XML has relevant field.
 */ 
"use strict";

var IDP_HOST_NAME = "{{{idp_host_name}}}";
var ID_PRIVACY_NS = "urn:mace:gakunin.jp:idprivacy";
var serializer_ = new XMLSerializer();

if (typeof zip_deflate === 'undefined' ||
    typeof zip_inflate === 'undefined') {
    console.error('zip_deflate() or zip_inflate() not available.');
}

// Pass-phrase used for encrypting RSA private key.
// In more practical situations, this should be
// dynamically supplied from SP.
//
// TODO: Support dynamic key encryption.
var PASS_PHRASE = "VSd6YvVL2m";

function deflate(inflated) {
    var result;
    try {
        result = $.base64.encode(
            zip_deflate(unescape(encodeURIComponent(inflated))));
    } catch (e) {
	console.error("Unexpected error in deflate(): " + e + "\n");
    }
    return result;
}

function inflate(deflated) {
    var result;
    try {
        result = decodeURIComponent(
            escape(zip_inflate($.base64.decode(deflated))));
    } catch (e) {
	console.error("Unexpected error in inflate(): " + e + "\n");
    }
    return result;
}

/*
 * Returns true when Agent extensions are successfully set.
 * Returns false when those extensions are already available
 * in Extensions field.
 */
function setupAgentExtensions(parser, request, privateKey, publicKeyContent) {
    var extensions_array = request.getElementsByTagNameNS(
        "urn:oasis:names:tc:SAML:2.0:protocol", "Extensions");
    if (extensions_array.length > 0) {
        // Extensions field was found in the incoming AuthnRequest.
        //
        // It implies that other protocols, typically DH or Cascade,
        // are already set up by Proxy.
        //
        // This method tries to find "protocol_type" field and
        // replace its value with "agent" to override the existing
        // protocol, remaining other values.
        //
        // IdP will just refer to the "protocol_type" at first,
        // ignoring rest of data associated with the other protocols.
        var extensions = extensions_array[0];
        console.log("Extensions found: " +
                    serializer_.serializeToString(extensions));

        // Replace existing protocol type if exists.
        var protocol_type_array = extensions.getElementsByTagNameNS(
            ID_PRIVACY_NS, "protocol_type");
        if (protocol_type_array.length > 0) {
            // If the first protocol is already "agent", it probably means
            // we already setup agent once, so we don't need to do that again.
            if (protocol_type_array[0] === "agent") {
                console.warn('protocol_type "agent" is defined already.');
                return false;
            }

            for (var i = 0; i < protocol_type_array.length; i++) {
                var protocol_type = protocol_type_array[i];
                protocol_type.textContent = "agent";
            }
        }

        insertAgentKeyTags(parser, extensions, privateKey, publicKeyContent);
        return true;
    } else {
        console.log("No Extensions field found. Insert new one.");
        var new_extensions = parser.parseFromString(
            '<samlp:Extensions '
                + 'xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol">'
                + '</samlp:Extensions>',
            "application/xml").firstChild;
        new_extensions.appendChild(parser.parseFromString(
            '<protocol_type xmlns="' + ID_PRIVACY_NS + '">'
                + 'agent'
                + '</protocol_type>',
            "application/xml").firstChild);
        insertAgentKeyTags(parser, new_extensions,
                           privateKey, publicKeyContent);
        request.appendChild(new_extensions);
        return true;
    }
}

function insertAgentKeyTags(parser, extensions, privateKey, publicKeyContent) {
    extensions.appendChild(parser.parseFromString(
        '<ag_pub_key xmlns="' + ID_PRIVACY_NS + '">'
            + publicKeyContent + '</ag_pub_key>',
        "application/xml").firstChild);
    extensions.appendChild(parser.parseFromString(
        '<ag_priv_key xmlns="' + ID_PRIVACY_NS + '">'
            + privateKey + '</ag_priv_key>',
        "application/xml").firstChild);
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function constructRsaPrivateKeyInPem(rsa) {
    var version = new KJUR.asn1.DERInteger({'int':0});
    var modulus = new KJUR.asn1.DERInteger({'int':rsa.n});
    var publicExponent = new KJUR.asn1.DERInteger({'int':rsa.e});
    var privateExponent = new KJUR.asn1.DERInteger({'int':rsa.d});
    var prime1 = new KJUR.asn1.DERInteger({'int':rsa.p});
    var prime2 = new KJUR.asn1.DERInteger({'int':rsa.q});
    var exponent1 = new KJUR.asn1.DERInteger({'int':rsa.dmp1});
    var exponent2 = new KJUR.asn1.DERInteger({'int':rsa.dmq1});
    var coefficient = new KJUR.asn1.DERInteger({'int':rsa.coeff});
    var seq = new KJUR.asn1.DERSequence(
        {'array': [version, modulus, publicExponent, privateExponent,
                   prime1, prime2, exponent1, exponent2, coefficient]});
    var b64 = hex2b64(seq.getEncodedHex()).replace(/(.{64})/g, "$1\n");
    if (!endsWith(b64, "\n")) {
        b64 += "\n";
    }
    var privateKeyPem = ("-----BEGIN RSA PRIVATE KEY-----\n"
                         + b64
                         + "-----END RSA PRIVATE KEY-----\n");
    return privateKeyPem;
}

function constructEncryptedRsaPrivateKeyInPem(rsa, password) {
    // Typo in the original code.
    return PKCS5PKEY.getEryptedPKCS5PEMFromRSAKey(rsa, password);
}

function constructRsaPublicKeyInPem(rsa) {
    return KJUR.asn1.x509.X509Util.getPKCS8PubKeyPEMfromRSAKey(rsa);
}

chrome.webRequest.onBeforeRequest.addListener(
    function(details) {
        var uri = URI(details.url);
        var queries = uri.query(true);
        if (!queries.hasOwnProperty('SAMLRequest')) {
            return null;
        }
        var inflated = inflate(queries['SAMLRequest']);
        if (!inflated) {
            return null;
        }

        var parser = new DOMParser();
        var request = (parser.parseFromString(inflated, "application/xml")
                       .firstChild);

        // Start creating RSA key pair
        var rsa = new RSAKey();
        rsa.generate(1024, "10001");
        // var privateKey = constructRsaPrivateKeyInPem(rsa);
        var privateKey = constructEncryptedRsaPrivateKeyInPem(rsa, PASS_PHRASE);
        var array = constructRsaPublicKeyInPem(rsa).split("\n");
        var publicKeyContent = "";
        // Remove header/footer for IdP.
        for (var i = 0; i < array.length; i++) {
            if (array[i].match("-+BEGIN (RSA )?PUBLIC KEY-+") != null
                || array[i].match("-+END (RSA )?PUBLIC KEY-+") != null) {
                // ignore it.
            } else {
                publicKeyContent += array[i] + "\n";
            }
        }

        console.log("privateKey: " + privateKey);
        console.log("publicKey(no header, no footer): " + publicKeyContent);

        if (!setupAgentExtensions(parser, request,
                                  privateKey, publicKeyContent)) {
            console.log("Agent protocol already there. "
                        + "Ignore the redirect request.");
            return null;
        }

        var serialized = serializer_.serializeToString(request);

        console.log("resultant request: " + serialized);

        uri.setQuery('SAMLRequest', deflate(serialized));
        console.log("resultant url:" + uri.toString());
        return {redirectUrl: uri.toString()};
    },
    {
        urls: [
            "*://" + IDP_HOST_NAME + "/*"
        ],
        types: ["main_frame", "sub_frame", "stylesheet", "script", "image",
                "object", "xmlhttprequest", "other"]
    },
    ["blocking"]
);  // chrome.webRequest.onBeforeRequest.addListener
