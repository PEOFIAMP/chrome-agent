#!/bin/bash
#
#   Copyright 2013 The University of Tokyo
#   
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#   
#   http://www.apache.org/licenses/LICENSE-2.0
#   
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ `pwd` != "$SOURCE_DIR" ]; then
  echo "Run in the same directory as setup.sh ($SOURCE_DIR/)"
  exit 1
fi

# Roughly check if setup is already finished or not.
if [ -f js/ext/inflate.js ] && [ -f js/ext/deflate.js ]; then
  echo "Setup seems already finished. Run cleanup.sh before retrying."
  exit 1
fi

if [ $# != 1 ]; then
  echo "Just one argument for idp name is required."
  exit 1
fi



wget -O js/ext/inflate.js http://www.onicos.com/staff/iz/amuse/javascript/expert/inflate.txt
wget -O js/ext/deflate.js http://www.onicos.com/staff/iz/amuse/javascript/expert/deflate.txt

wget -O js/ext/yahoo-2.9.0-min.js http://yui.yahooapis.com/2.9.0/build/yahoo/yahoo-min.js
wget -O CryptoJS.3.1.2.zip https://crypto-js.googlecode.com/files/CryptoJS%20v3.1.2.zip
unzip -d js/ext/crypto-js/ CryptoJS.3.1.2.zip

echo
echo "\"$1\" will be used for a target IdP."
sed -i -e "s/{{{idp_host_name}}}/$1/" js/main.js
sed -i -e "s/{{{idp_host_name}}}/$1/" manifest.json
echo "$1" > .idp_host_name